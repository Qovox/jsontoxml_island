Dans le dossier src vous pourrez retrouver :

Les 6 classes java qui contiennent le code du programme pour construire le fichier xml, ainsi que celui qui fait l'analyse des donn�es xml construites.

- Lancer Launcher (dans un ide) en lui passant en param�tre un fichier JSON (situ� dans le dossier resource imp�rativement) pour cr�er le dossier XML associ�.
- Lancer LauncherStats pour effectuer l'analyse des donn�es du fichier island.xml pr�c�demment produit par Launcher.

Dans le dossier resource vous pourrez retrouver :

- Explorer_IABC.json : un fichier log basique pour tester le programme
- Explorer_IABC_Excpetion.json : un fichier log un peu plus complet et particulier pour tester un peu plus en profondeur le fichier xml produit.
- island.css : le fichier css associ� au fichier r�sultat island.xml
- island.dtd : la dtd associ�e au fichier r�sultat island.xml
- island.xml : le fichier r�sultat du programme
- jsonSchema.xml : le jsonSchema des fichiers log json Island utilis�s.
- xmlModel.xml : un fichier jouet xml qui contient de mani�re synth�tique tout ce que l'on pourrait retrouver dans un fichier r�sultat island.xml produit par le programme.
- xmlSchema.xsd : le xmlSchema des fichiers xml produits par le programme.