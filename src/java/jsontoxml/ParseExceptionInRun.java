package jsontoxml;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

class ParseExceptionInRun {

    ParseExceptionInRun(JSONObject exception, Document xmlDoc) throws JSONException {
        parseException(exception, xmlDoc);
    }

    private void parseException(JSONObject exception, Document xmlDoc) throws JSONException {
        Node root = xmlDoc.getDocumentElement();

        Element exceptionXml = xmlDoc.createElement("exception");
        Element exceptionType = xmlDoc.createElement("exception_type");
        Element message = xmlDoc.createElement("message");

        exceptionType.appendChild(xmlDoc.createTextNode((String) exception.getJSONArray("exception").get(0)));
        message.appendChild(xmlDoc.createTextNode((String) exception.getJSONArray("message").get(0)));

        exceptionXml.appendChild(exceptionType);
        exceptionXml.appendChild(message);

        root.appendChild(exceptionXml);
    }

}
