package jsontoxml;

import com.sun.org.apache.xerces.internal.dom.ProcessingInstructionImpl;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;

public class IslandJsonToXML {

    private String filePath;
    private JSONArray traceLog;
    private Document xmlDoc;

    public IslandJsonToXML(String filePath) throws JSONException {
        this.filePath = filePath;
        initialization();
        parse();
    }

    /**
     * Initialize traceLog from fileName/filePath and xml document to fill.
     */
    private void initialization() {
        try {
            ClassLoader classLoader = getClass().getClassLoader();
            File file = new File(classLoader.getResource(filePath).getFile());
            FileReader fileReader = new FileReader(file);
            JSONTokener tokener = new JSONTokener(fileReader);
            traceLog = new JSONArray(tokener);
            DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
            xmlDoc = documentBuilder.newDocument();
            ProcessingInstructionImpl xmlDocProcessingInstruction = (ProcessingInstructionImpl)
                    xmlDoc.createProcessingInstruction("xml-stylesheet",
                            "type=\"text/css\" href=\"island.css\"\n");
            Element root = xmlDoc.createElement("island");
            xmlDoc.appendChild(root);
            xmlDoc.insertBefore(xmlDocProcessingInstruction, root);
        } catch (FileNotFoundException | JSONException | ParserConfigurationException e) {
            e.printStackTrace();
        }
    }

    /**
     * Completely parse the JSON file tracelog.
     */
    private void parse() throws JSONException {
        int id = 1;
        new ParseHeader(((JSONObject) traceLog.get(0)).getJSONObject("data"), xmlDoc);
        for (int i = 1; i < traceLog.length(); i += 2) {
            JSONObject expectDecision = traceLog.getJSONObject(i);
            expectDecision = expectDecision.getJSONObject("data");
            JSONObject expectResult = null;
            if (i + 1 < traceLog.length())
                expectResult = traceLog.getJSONObject(i + 1).getJSONObject("data");
            if (expectDecision.has("action") && (expectResult != null && expectResult.has("cost"))) {
                new ParseDecision(expectDecision, expectResult, xmlDoc, id);
                id++;
            } else if (expectResult != null && expectResult.has("exception")) {
                new ParseDecision(expectDecision, xmlDoc, id);
                new ParseExceptionInRun(expectResult, xmlDoc);
                id++;
            }
        }
        save();
    }

    /**
     * Save the xml document.
     */
    private void save() {
        try {
            TransformerFactory tFactory = TransformerFactory.newInstance();
            Transformer transformer = tFactory.newTransformer();
            transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
            transformer.setOutputProperty(OutputKeys.DOCTYPE_SYSTEM, "island.dtd");
            StreamResult result = new StreamResult(new FileOutputStream(new File("src/resources/island.xml")));
            transformer.transform(new DOMSource(xmlDoc), result);
        } catch (FileNotFoundException | TransformerException e) {
            e.printStackTrace();
        }
    }

}
