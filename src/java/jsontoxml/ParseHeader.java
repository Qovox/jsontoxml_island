package jsontoxml;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

class ParseHeader {

    ParseHeader(JSONObject header, Document xmlDoc) throws JSONException {
        parseHeader(header, xmlDoc);
    }

    private void parseHeader(JSONObject header, Document xmlDoc) throws JSONException {
        Node root = xmlDoc.getDocumentElement();

        Element headerXML = xmlDoc.createElement("header_island");
        Element heading = xmlDoc.createElement("heading");
        Element men = xmlDoc.createElement("men");
        Element budget = xmlDoc.createElement("budget");
        Element contract = xmlDoc.createElement("contract");

        heading.appendChild(xmlDoc.createTextNode(header.getString("heading").toUpperCase()));
        men.appendChild(xmlDoc.createTextNode(String.valueOf(header.getInt("men"))));
        budget.appendChild(xmlDoc.createTextNode(String.valueOf(header.getInt("budget"))));

        JSONArray contracts = header.getJSONArray("contracts");
        for (int i = 0; i < contracts.length(); i++) {
            JSONObject contractElement = (JSONObject) contracts.get(i);

            Element contractElementXml = xmlDoc.createElement("contract_element");
            Element resource = xmlDoc.createElement("contract_resource");
            Element amount = xmlDoc.createElement("contract_amount");

            resource.appendChild(xmlDoc.createTextNode(contractElement.getString("resource").toUpperCase()));
            amount.appendChild(xmlDoc.createTextNode(String.valueOf(contractElement.getInt("amount"))));

            contractElementXml.appendChild(resource);
            contractElementXml.appendChild(amount);
            contract.appendChild(contractElementXml);
        }

        headerXML.appendChild(heading);
        headerXML.appendChild(men);
        headerXML.appendChild(budget);
        headerXML.appendChild(contract);

        root.appendChild(headerXML);
    }

}
