package jsontoxml;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import java.util.Iterator;

class ParseDecision {

    private static final String EXTRA = "extras";
    private static final String DIRECTION = "direction";
    private static final String RANGE = "range";
    private static final String CREEK = "creek";
    private static final String CREEKS = "creeks";
    private static final String PEOPLE = "people";
    private static final String SITES = "sites";
    private static final String AMOUNT = "amount";
    private static final String RESOURCE = "resource";
    private static final String RESOURCES = "resources";
    private static final String REPORT = "report";

    private Document xmlDoc;
    private Element decisionXml;
    private String name;

    ParseDecision(JSONObject decision, JSONObject result, Document xmlDoc, int id) throws JSONException {
        this.xmlDoc = xmlDoc;
        parseDecision(decision, id);
        parseResult(result, xmlDoc);
        Node root = xmlDoc.getDocumentElement();
        root.appendChild(decisionXml);
    }

    ParseDecision(JSONObject decision, Document xmlDoc, int id) throws JSONException {
        this.xmlDoc = xmlDoc;
        parseDecision(decision, id);
        Node root = xmlDoc.getDocumentElement();
        root.appendChild(decisionXml);
    }

    private void parseDecision(JSONObject decision, int id) throws JSONException {
        decisionXml = xmlDoc.createElement("decision");
        Element idXml = xmlDoc.createElement("id");
        idXml.appendChild(xmlDoc.createTextNode(String.valueOf(id)));
        decisionXml.appendChild(idXml);
        Element nameXml = xmlDoc.createElement("name");

        name = decision.getString("action").toUpperCase();
        nameXml.appendChild(xmlDoc.createTextNode(name));
        decisionXml.appendChild(nameXml);

        if (decision.has("parameters")) {
            JSONObject parameters = decision.getJSONObject("parameters");

            if ("transform".equals(decision.getString("action")))
                fillParameterOnTransform(parameters, decisionXml);
            else fillParameter(parameters);
        }
    }

    private void parseResult(JSONObject result, Document xmlDoc) throws JSONException {
        Element cost = xmlDoc.createElement("cost");

        cost.appendChild(xmlDoc.createTextNode(String.valueOf(result.getInt("cost"))));
        decisionXml.appendChild(cost);

        if (result.has(EXTRA)) {
            Element extra = xmlDoc.createElement(EXTRA);
            JSONObject extras = result.getJSONObject(EXTRA);

            if ("ECHO".equals(name))
                fillExtraOnEcho(extras, extra);
            else if ("SCAN".equals(name))
                fillExtraOnScan(extras, extra);
            else if ("SCOUT".equals(name))
                fillExtraOnScout(extras, extra);
            else if ("GLIMPSE".equals(name))
                fillExtraOnGlimpse(extras, extra);
            else if ("EXPLORE".equals(name))
                fillExtraOnExplore(extras, extra);
            else if ("EXPLOIT".equals(name))
                fillExtraOnExploit(extras, extra);
            else if ("TRANSFORM".equals(name))
                fillExtraOnTransform(extras, extra);

            decisionXml.appendChild(extra);
        }
    }

    private void fillParameter(JSONObject parameters) throws JSONException {
        String[] parametersList = {DIRECTION, CREEK, PEOPLE, RANGE, RESOURCE};

        for (int i = 0; i < parameters.length(); i++) {
            Element parameter = xmlDoc.createElement("parameter");

            if (parameters.has(DIRECTION) && !"".equals(parametersList[0])) {
                parameter.appendChild(xmlDoc.createTextNode("Direction : " + parameters.getString(DIRECTION).toUpperCase()));
                parametersList[0] = "";
            } else if (parameters.has(CREEK) && !"".equals(parametersList[1])) {
                parameter.appendChild(xmlDoc.createTextNode("Creek  : " + parameters.getString(CREEK)));
                parametersList[1] = "";
            } else if (parameters.has(PEOPLE) && !"".equals(parametersList[2])) {
                parameter.appendChild(xmlDoc.createTextNode("People : " + String.valueOf(parameters.getInt(PEOPLE))));
                parametersList[2] = "";
            } else if (parameters.has(RANGE) && !"".equals(parametersList[3])) {
                parameter.appendChild(xmlDoc.createTextNode("Range : " + String.valueOf(parameters.getInt(RANGE))));
                parametersList[3] = "";
            } else if (parameters.has(RESOURCE) && !"".equals(parametersList[4])) {
                parameter.appendChild(xmlDoc.createTextNode(parameters.getString(RESOURCE).toUpperCase()));
                parametersList[4] = "";
            }

            decisionXml.appendChild(parameter);
        }
    }

    /**
     * Handle transform action
     *
     * @param parameters  the json object to parse
     * @param decisionXml the node whose append children.
     */
    private void fillParameterOnTransform(JSONObject parameters, Element decisionXml) throws JSONException {
        for (Iterator it = parameters.keys(); it.hasNext(); ) {
            Element parameter = xmlDoc.createElement("parameter");
            String resources = (String) it.next();
            parameter.appendChild(xmlDoc.createTextNode(resources + " : " + String.valueOf(parameters.getInt(resources))));
            decisionXml.appendChild(parameter);
        }
    }

    private void fillExtraOnEcho(JSONObject extras, Element extra) throws JSONException {
        Element found = xmlDoc.createElement("found");
        Element range = xmlDoc.createElement(RANGE);

        found.appendChild(xmlDoc.createTextNode(extras.getString("found").toUpperCase()));
        range.appendChild(xmlDoc.createTextNode(String.valueOf(extras.getInt(RANGE))));

        extra.appendChild(found);
        extra.appendChild(range);
    }

    private void fillExtraOnScan(JSONObject extras, Element extra) throws JSONException {
        JSONArray biomesList = extras.getJSONArray("biomes");
        Element biomes = xmlDoc.createElement("biomes");

        for (int i = 0; i < biomesList.length(); i++) {
            Element biome = xmlDoc.createElement("biome");
            biome.appendChild(xmlDoc.createTextNode((String) biomesList.get(i)));
            biomes.appendChild(biome);
        }

        extra.appendChild(biomes);

        fillNestedAttribute(extras, extra, CREEKS, CREEK);
        fillNestedAttribute(extras, extra, SITES, "site");
    }

    private void fillExtraOnScout(JSONObject extras, Element extra) throws JSONException {
        Element altitude = xmlDoc.createElement("altitude");
        altitude.appendChild(xmlDoc.createTextNode(String.valueOf(extras.getInt("altitude"))));
        extra.appendChild(altitude);
        fillNestedAttribute(extras, extra, RESOURCES, RESOURCE);
    }

    private void fillExtraOnGlimpse(JSONObject parameters, Element extra) throws JSONException {
        Element askedRange = xmlDoc.createElement("asked_range");
        askedRange.appendChild(xmlDoc.createTextNode(String.valueOf(parameters.getInt("asked_range"))));
        extra.appendChild(askedRange);

        if (parameters.getJSONArray(REPORT).length() != 0) {
            JSONArray reportOnRange = parameters.getJSONArray(REPORT);
            Element report = xmlDoc.createElement(REPORT);

            for (int i = 0; i < reportOnRange.length(); i++) {
                JSONArray reportOnRangeBiomeSet = reportOnRange.getJSONArray(i);
                Element reportOnRangeXml = xmlDoc.createElement("report_on_range");

                if (reportOnRangeBiomeSet.length() != 0) {
                    Element range = xmlDoc.createElement(RANGE);
                    range.appendChild(xmlDoc.createTextNode(String.valueOf(i)));
                    reportOnRangeXml.appendChild(range);

                    for (int j = 0; j < reportOnRangeBiomeSet.length(); j++) {
                        Element biome = xmlDoc.createElement("biome");

                        if (reportOnRangeBiomeSet.get(j) instanceof JSONArray) {
                            JSONArray reportObject = reportOnRangeBiomeSet.getJSONArray(j);

                            Element percentage = xmlDoc.createElement("percentage");

                            biome.appendChild(xmlDoc.createTextNode(((String) reportObject.get(0)).toUpperCase()));
                            percentage.appendChild(xmlDoc.createTextNode(String.valueOf(reportObject.get(1))));

                            reportOnRangeXml.appendChild(biome);
                            reportOnRangeXml.appendChild(percentage);
                        } else {
                            biome.appendChild(xmlDoc.createTextNode(((String) reportOnRangeBiomeSet.get(j)).toUpperCase()));
                            reportOnRangeXml.appendChild(biome);
                        }
                        report.appendChild(reportOnRangeXml);
                    }

                    extra.appendChild(report);
                }
            }
        }
    }

    private void fillExtraOnExplore(JSONObject parameters, Element extra) throws JSONException {
        if (parameters.getJSONArray(RESOURCES).length() != 0) {
            JSONArray resourcesOnExplore = parameters.getJSONArray(RESOURCES);

            for (int i = 0; i < resourcesOnExplore.length(); i++) {
                JSONObject resourceExploited = resourcesOnExplore.getJSONObject(i);
                Element resourcesOnExploreXml = xmlDoc.createElement("resources_on_explore");

                Element amount = xmlDoc.createElement(AMOUNT);
                Element resource = xmlDoc.createElement(RESOURCE);
                Element cond = xmlDoc.createElement("cond");

                amount.appendChild(xmlDoc.createTextNode(resourceExploited.getString(AMOUNT)));
                resource.appendChild(xmlDoc.createTextNode(resourceExploited.getString(RESOURCE).toUpperCase()));
                cond.appendChild(xmlDoc.createTextNode(resourceExploited.getString("cond").toUpperCase()));

                resourcesOnExploreXml.appendChild(amount);
                resourcesOnExploreXml.appendChild(resource);
                resourcesOnExploreXml.appendChild(cond);
                extra.appendChild(resourcesOnExploreXml);
            }
        }

        if (parameters.getJSONArray("pois").length() != 0) {
            JSONArray pois = parameters.getJSONArray("pois");
            Element poisXml = xmlDoc.createElement("pois");

            for (int i = 0; i < pois.length(); i++) {
                JSONObject poisObject = pois.getJSONObject(i);
                Element kind = xmlDoc.createElement("kind");
                Element poisId = xmlDoc.createElement("pois_id");

                kind.appendChild(xmlDoc.createTextNode(poisObject.getString("kind").toUpperCase()));
                poisId.appendChild(xmlDoc.createTextNode(poisObject.getString("id").toUpperCase()));

                poisXml.appendChild(kind);
                poisXml.appendChild(poisId);
            }

            extra.appendChild(poisXml);
        }
    }

    private void fillExtraOnExploit(JSONObject parameters, Element extra) throws JSONException {
        Element amount = xmlDoc.createElement(AMOUNT);
        amount.appendChild(xmlDoc.createTextNode(String.valueOf(parameters.getInt(AMOUNT))));
        extra.appendChild(amount);
    }

    private void fillExtraOnTransform(JSONObject parameters, Element extra) throws JSONException {
        Element production = xmlDoc.createElement("production");
        Element kindOfProd = xmlDoc.createElement("kind_of_prod");

        production.appendChild(xmlDoc.createTextNode(String.valueOf(parameters.getInt("production"))));
        kindOfProd.appendChild(xmlDoc.createTextNode(parameters.getString("kind").toUpperCase()));

        extra.appendChild(production);
        extra.appendChild(kindOfProd);
    }

    private void fillNestedAttribute(JSONObject extras, Element extra, String type, String nestedType) throws JSONException {
        if (extras.getJSONArray(type).length() != 0) {
            JSONArray typeList = extras.getJSONArray(type);
            Element typeXml = xmlDoc.createElement(type);

            for (int i = 0; i < typeList.length(); i++) {
                Element nestedTypeXml = xmlDoc.createElement(nestedType);
                nestedTypeXml.appendChild(xmlDoc.createTextNode((String) typeList.get(i)));
                typeXml.appendChild(nestedTypeXml);
            }

            extra.appendChild(typeXml);
        }
    }

}
