package launcher;

import jsontoxml.IslandJsonToXML;
import org.json.JSONException;

public class Launcher {

    private Launcher() {
    }

    public static void main(String[] args) throws JSONException {
        //Change to Explorer_IABC.json to have a tracelog ending normally
        //Explorer_IABC_Exception.json allows the parse to parse everything he can
        new IslandJsonToXML("Explorer_IABC_Exception.json");
    }

}
