package launcher;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.traversal.DocumentTraversal;
import org.w3c.dom.traversal.NodeFilter;
import org.w3c.dom.traversal.NodeIterator;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class LauncherStats {

    private static int nbAction = 0;
    private static int nbActionAerienne = 0;
    private static int nbActionTerrestre = 0;
    private static int nbFly = 0;
    private static int nbHeading = 0;
    private static int nbEcho = 0;
    private static int nbScan = 0;
    private static int nbLand = 0;
    private static int nbStop = 0;
    private static int nbMoveTo = 0;
    private static int nbScout = 0;
    private static int nbGlimpse = 0;
    private static int nbExplore = 0;
    private static int nbExploit = 0;
    private static int nbTransform = 0;

    private static boolean hasLand = false;

    private LauncherStats() {
    }

    public static void main(String[] args) throws ParserConfigurationException, IOException, SAXException {

        List<Integer> costs = new ArrayList<>();

        DocumentBuilderFactory dBF = DocumentBuilderFactory.newInstance();
        DocumentBuilder dB = dBF.newDocumentBuilder();
        Document xmlDoc = dB.parse("src/resources/island.xml");
        DocumentTraversal traversal = (DocumentTraversal) xmlDoc;
        NodeIterator nodeIterator = traversal.createNodeIterator(xmlDoc.getDocumentElement(),
                NodeFilter.SHOW_ELEMENT, null, true);

        for (Node n = nodeIterator.nextNode(); n != null; n = nodeIterator.nextNode()) {

            if (n.getNodeName().equals("cost"))
                costs.add(Integer.parseInt(n.getTextContent().substring(7)));

            else if (n.getNodeName().equals("name")) {
                nbAction++;
                if (hasLand)
                    nbActionTerrestre++;
                else nbActionAerienne++;
                switch (n.getTextContent()) {
                    case "FLY":
                        nbFly++;
                        break;
                    case "HEADING":
                        nbHeading++;
                        break;
                    case "ECHO":
                        nbEcho++;
                        break;
                    case "SCAN":
                        nbScan++;
                        break;
                    case "STOP":
                        nbStop++;
                        break;
                    case "LAND":
                        nbLand++;
                        hasLand = true;
                        break;
                    case "MOVE_TO":
                        nbMoveTo++;
                        break;
                    case "SCOUT":
                        nbScout++;
                        break;
                    case "GLIMPSE":
                        nbGlimpse++;
                        break;
                    case "EXPLORE":
                        nbExplore++;
                        break;
                    case "EXPLOIT":
                        nbExploit++;
                        break;
                    case "TRANSFORM":
                        nbTransform++;
                        break;
                }
            }
        }

        String stats = "Total d'actions effectuées : " + nbAction + "\n\n" +
                "    Détails des action :\n     - FLY : " + nbFly +
                "\n     - HEADING : " + nbHeading +
                "\n     - ECHO : " + nbEcho +
                "\n     - SCAN : " + nbScan +
                "\n     - STOP : " + nbStop +
                "\n     - LAND : " + nbLand +
                "\n     - MOVE_TO : " + nbMoveTo +
                "\n     - SCOUT : " + nbScout +
                "\n     - GLIMPSE : " + nbGlimpse +
                "\n     - EXPLORE : " + nbExplore +
                "\n     - EXPLOIT : " + nbExploit +
                "\n     - TRANSFORM : " + nbTransform + "\n" +
                "\n     - ACTION PHASE AÉRIENNE : " + nbActionAerienne +
                "\n     - ACTION PHASE TERRESTRE : " + nbActionTerrestre + "\n\n" +
                "Coût total des actions : " + totalCost(costs) + "\n\n" +
                "    Détails des coûts :\n     - MIN : " + minCost(costs) +
                "\n     - MAX : " + maxCost(costs) +
                "\n     - MOYENNE : " + moyCost(costs) +
                "\n     - VARIANCE : " + varCost(costs) +
                "\n     - ÉCART-TYPE : " + Math.sqrt(varCost(costs));

        System.out.println(stats);
    }

    private static int totalCost(List<Integer> costs) {
        int result = 0;
        for (Integer cost : costs) {
            result += cost;
        }
        return result;
    }

    private static int minCost(List<Integer> costs) {
        int result = costs.get(0);
        for (Integer cost : costs) {
            if (cost < result)
                result = cost;
        }
        return result;
    }

    private static int maxCost(List<Integer> costs) {
        int result = costs.get(0);
        for (Integer cost : costs) {
            if (cost > result)
                result = cost;
        }
        return result;
    }

    private static double moyCost(List<Integer> costs) {
        double result = 0;
        for (Integer cost : costs) {
            result += cost;
        }
        return result / costs.size();
    }

    private static double moyCostSquared(List<Integer> costs) {
        double result = 0;
        for (Integer cost : costs) {
            result += cost * cost;
        }
        return result != 0 ? result / costs.size() : 0;
    }

    private static double varCost(List<Integer> costs) {
        return moyCostSquared(costs) - (moyCost(costs) * moyCost(costs));
    }

}
